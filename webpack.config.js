var path = require('path');
var webpack = require("webpack");
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var prod = process.argv.indexOf('-p') !== -1;
var extractSass = new ExtractTextPlugin({
  filename: 'css/main.css',
  disable: !prod
});
var HtmlWebpackPlugin = require('html-webpack-plugin');
var FaviconsWebpackPlugin = require('favicons-webpack-plugin');
var SpriteLoaderPlugin = require('svg-sprite-loader/plugin');
var CleanWebpackPlugin = require('clean-webpack-plugin');
var autoprefixer = require('autoprefixer');

var postcssPlugins = []
if (prod) {
  postcssPlugins = [
    autoprefixer({
      browsers:['last 2 version']
    })
  ]
}

var src = path.join(__dirname, 'src');
module.exports = {
  entry: './src/main.js',

  output: {
    filename: 'js/bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },


  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    quiet: false,
    // host: '192.168.0.200',
    port: 3000
  },

  stats: {
    assets: false,
    colors: true,
    version: false,
    hash: true,
    timings: true,
    chunks: false,
    chunkModules: false
  },

  module: {
    rules: [
      {
        test: /\.pug$/,
        use: [
          {
            loader: 'html-loader',
            options: {
              attrs: ['img:src', 'use:xlink:href', 'source:src', 'a:href', 'div:data-src'],
               minimize: false
            }
          },
          {
            loader: 'pug-html-loader',
            options: {
              doctype: 'html',
              basedir: path.resolve(__dirname, 'src/blocks'),
              pretty: true
            }
          }
        ]
      },
      {
        test: /\.scss$/,
        use: extractSass.extract({
          use: [
          {
            loader: "css-loader",
            options: {
                sourceMap: true,
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              plugins: postcssPlugins,
              sourceMap: true,
            }
          },
          {
            loader: "sass-loader",
            options: {
                sourceMap: true,
            }
          }],
          fallback: "style-loader",
          publicPath:'../'
        })
      },
      {
        test: /\.(jpg|png)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'img/'
            }
          },
          {
            loader: 'image-webpack-loader',
            options: {
              optipng: {
                optimizationLevel: 7,
              },
              pngquant: {
                quality: '70-90',
                speed: 4
              },
              mozjpeg: {
                progressive: true,
                quality: 80
              }
            }
          }
        ]
      },
      {
        test: /\.mp3$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'audio/'
            }
          }
        ]
      },
      {
        test: /\.svg$/,
        use: [
          {
            loader: 'svg-sprite-loader',
            options: {
              extract: true,
              spriteFilename: 'img/sprite.svg'
            }
          },
          {
            loader: 'svgo-loader',
            options: {
              plugins: [
                {removeTitle: true},
                {removeStyleElement: true},
                {removeScriptElement: true}
              ]
            }
          }
        ]
      },
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['env']
          }
        }
      }
    ]
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new CleanWebpackPlugin(['dist'], {
      dry: !prod
    }),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      IScroll: 'iscroll',
    }),
    extractSass,
    new HtmlWebpackPlugin({
      template: 'src/templates/index.pug',
      filename: 'index.html'
    }),
    new HtmlWebpackPlugin({
      template: 'src/templates/article.pug',
      filename: 'article.html'
    }),
    // new HtmlWebpackPlugin({
    //   template: 'src/templates/catalog.pug',
    //   filename: 'catalog.html'
    // }),
    // new HtmlWebpackPlugin({
    //   template: 'src/templates/services.pug',
    //   filename: 'services.html'
    // }),
    // new HtmlWebpackPlugin({
    //   template: 'src/templates/service.pug',
    //   filename: 'service.html'
    // }),
    // new HtmlWebpackPlugin({
    //   template: 'src/templates/news.pug',
    //   filename: 'news.html'
    // }),
    // new HtmlWebpackPlugin({
    //   template: 'src/templates/news-entry.pug',
    //   filename: 'news-entry.html'
    // }),
    // new HtmlWebpackPlugin({
    //   template: 'src/templates/about.pug',
    //   filename: 'about.html'
    // }),
    // new HtmlWebpackPlugin({
    //   template: 'src/templates/contacts.pug',
    //   filename: 'contacts.html'
    // }),
    // new HtmlWebpackPlugin({
    //   template: 'src/templates/projects.pug',
    //   filename: 'projects.html'
    // }),
    // new HtmlWebpackPlugin({
    //   template: 'src/templates/project.pug',
    //   filename: 'project.html'
    // }),
    // new FaviconsWebpackPlugin({
    //   logo: './src/img/favicon.png',
    //   prefix: 'img/',
    //   emitStats: false,
    //   statsFilename: 'iconstats-[hash].json',
    //   persistentCache: true,
    //   inject: true,
    //   background: '#fff',
    //   icons: {
    //     android: false,
    //     appleIcon: true,
    //     appleStartup: false,
    //     coast: false,
    //     favicons: true,
    //     firefox: false,
    //     opengraph: false,
    //     twitter: false,
    //     yandex: false,
    //     windows: false
    //   }
    // }),
    new SpriteLoaderPlugin()
  ]
};
