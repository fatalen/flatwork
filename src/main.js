/*styles*/
import './main.scss';

/*vendor*/
import 'jquery';
import 'holderjs';
import 'iscroll';
import 'jquery-drawer';
import 'magnific-popup';
// import Shuffle from 'shufflejs';

/*blocks*/
import './blocks/button/button.js';
import './blocks/icon/icon.js';
import './blocks/page/page.js';
// import './blocks/song/song.js';
import './blocks/swiper/swiper.js';

/*global*/
$(document).ready(function() {
  $('.drawer').drawer();

  $('.gal').each(function() {
    $(this).magnificPopup({
      delegate: 'a.gal__item',
      removalDelay: 300,
      type: 'image',
      image: {
        markup: '<div class="mfp-figure">'+
                  '<div class="mfp-close"></div>'+
                  '<div class="mfp-img"></div>'+
                  '<div class="mfp-bottom-bar">'+
                    '<div class="mfp-title"></div>'+
                    '<div class="mfp-counter"></div>'+
                  '</div>'+
                '</div>',
        titleSrc: 'title'
      },
      gallery: {
        enabled:true,
        tPrev: 'назад',
        tNext: 'вперед',
        tCounter: '<span class="mfp-counter">%curr% из %total%</span>'
      }
    });
  });

  $(window).on('load', function () {

  });
});

/*project*/
