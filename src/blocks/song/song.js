import 'howler';

$(document).ready(function() {
  var playlist = [];
  var progressInterval;

  $('.song').each(function() {
    var $this = $(this);
    var play = $this.find('.song__play');
    var pause = $this.find('.song__pause');
    var progress = $this.find('.song__progress');
    var songSeek = $this.find('.song__seek');
    var sound = new Howl({
      src: $this.data('src'),
      preload: false
    });
    var duration;
    var width = 0;
    var loadBegin = false

    playlist.push(sound);

    $this.on('mouseover', function () {
      if (!loadBegin) {
        sound.load();
        songSeek.html('загрузка.. /');
      }
    });

    sound.on('load', function(){
      loadBegin = true;
      duration = sound.duration();
      var minutes = Math.floor(duration / 60);
      var seconds = Math.floor(duration - minutes * 60);
      $this.find('.song__duration').html(' ' + minutes + ':' + ('0' + seconds).slice(-2));
      songSeek.html('0:00 /');
    });

    sound.on('play', function(){
      var this1 = this;
      progressInterval = setInterval(function(){
        var seek = this1.seek();
        var minutes = Math.floor(seek / 60);
        var seconds = Math.floor(seek - minutes * 60);
        songSeek.html(minutes + ':' + ('0'+seconds).slice(-2) + ' /');
        width = seek/duration*100+'%';
        progress.width(width);
      }, 100);
    });

    sound.on('end', function(){
      clearInterval(progressInterval);
      pause.addClass('hidden');
      play.removeClass('hidden');
      progress.width(0);
      songSeek.html('0:00 /');
    });

    play.click(function(){
      if (!sound.playing()) {
        $.each(playlist, function(){
          this.pause();
        });
        clearInterval(progressInterval);
        sound.play();
        $('.song__play').removeClass('hidden');
        $('.song__pause').addClass('hidden');
        play.addClass('hidden');
        pause.removeClass('hidden');
      }
    });

    pause.click(function(){
      if (sound.playing()) {
        sound.pause();
        clearInterval(progressInterval);
        pause.addClass('hidden');
        play.removeClass('hidden');
      }
    });
  });
});
