$('.button--ripple').on('click', function(e){
    var btnOffset = $(this).offset(),
    xPos = e.pageX - btnOffset.left - 25,
    yPos = e.pageY - btnOffset.top - 25,
    $div = $('<div class="button__effect" style="top:' + yPos + 'px; left:' + xPos + 'px;"></div>');
    $div.appendTo($(this));
    window.setTimeout(function(){
      $div.remove();
    }, 2000);
});
