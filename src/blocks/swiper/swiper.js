import Swiper from '../../../node_modules/swiper/dist/js/swiper.min.js';

$(document).ready(function() {
  // var mySwiper = new Swiper('.swiper-container', {
  //   autoHeight: true,
  //   loop: true,
  //   effect: 'fade',
  //   speed: 500,
  //   autoplay: {
  //     delay: 5000,
  //   },
  //   navigation: {
  //     nextEl: '.swiper-button-next',
  //     prevEl: '.swiper-button-prev',
  //   },
  //   pagination: {
  //     el: '.swiper-pagination',
  //     type: 'bullets',
  //     dynamicBullets: true,
  //     clickable: true
  //   },
  // });

  var swiperFull = new Swiper('.swiper--full', {
    autoHeight: true,
    loop: true,
    // effect: 'fade',
    speed: 1000,
    autoplay: {
      delay: 5000,
    },
    // navigation: {
    //   nextEl: '.swiper-button-next',
    //   prevEl: '.swiper-button-prev',
    // },
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
      dynamicBullets: true,
      clickable: true
    },
  });

  var swiperCarousel = new Swiper('.swiper--carousel', {
    slidesPerView: 6,
    spaceBetween: 30,
    // loop: true,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    breakpoints: {
      1200: {
        slidesPerView: 4,
        spaceBetween: 20,
      },
      768: {
        slidesPerView: 3,
        spaceBetween: 10,
      },
      576: {
        slidesPerView: 2,
        spaceBetween: 10,
      }
    }
  });

  $(window).on('load', function () {
    if (swiperFull.width) {
      swiperFull.update();
    }

    if (swiperCarousel.width) {
      swiperCarousel.update();
    }
  });
});
