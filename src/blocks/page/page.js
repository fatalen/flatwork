$('.js-agree').on('click', function(e){
  e.preventDefault();

  localStorage.setItem('personal-agreement', true);
  $('.page__message').fadeOut();
});

if (!localStorage.getItem('personal-agreement')) {
  $('.page__message').addClass('page__message--visible');
}
